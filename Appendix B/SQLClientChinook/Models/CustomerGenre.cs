﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Models
{
    // Model class for a customers most popular genre.
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public int GenreId { get; set; }
        public string GenreName { get; set; }
    }
}

