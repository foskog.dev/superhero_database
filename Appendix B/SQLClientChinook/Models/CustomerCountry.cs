﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Models
{
    // Model class for the number of customers from each country in the chinook database.
    public class CustomerCountry
    {
        public int CustomerCount { get; set; }
        public string CountryName { get; set; }
    }
}
