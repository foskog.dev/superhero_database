﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Models
{
    // Model class that orders customers by their spending habits. Top customers first.
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public decimal TotalSpent { get; set; }
    }
}
