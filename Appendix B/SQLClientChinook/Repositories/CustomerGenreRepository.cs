﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Repository for CustomerGenre objects. Used to get a single customers most popular genre.
    /// </summary>
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Takes a customer and checks what their most popular genre is. In case of a tie it will return multiple top genres.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>List containing either a single top customer genre. Or multple genres in case of a tie.</returns>
        public IEnumerable<CustomerGenre> GetCustomerTopGenre(int id)
        {

            List<CustomerGenre> topGenre = new List<CustomerGenre>();
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT TOP 1 WITH TIES Genre.Name, Track.GenreId, COUNT(Track.GenreId) AS GenreCount " +
                    "FROM Customer " +
                    "INNER JOIN Invoice On Customer.CustomerId = Invoice.CustomerId " +
                    "INNER JOIN Track On Invoice.InvoiceId = Track.TrackId " +
                    "INNER JOIN Genre On Track.GenreId = Genre.GenreId WHERE Customer.CustomerId = @CustomerId " +
                    "GROUP BY Track.GenreId, Genre.Name " +
                    "ORDER BY genreCount DESC";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", id);

                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    CustomerGenre customerGenre = new CustomerGenre
                    {
                        CustomerId = id,
                        GenreName = reader.GetString(0),
                        GenreId = reader.GetInt32(1),

                    };
                    topGenre.Add(customerGenre);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return topGenre;
        }
    }
}
