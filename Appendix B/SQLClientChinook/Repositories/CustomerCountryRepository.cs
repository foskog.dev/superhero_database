﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Repository for CustomerCountry objects. Used to get the amount of customers in a country.
    /// </summary>
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Gets the amount of customers in the database in each registered country.
        /// </summary>
        /// <returns>List of CustomerCountry Objects</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountry()
        {

            List<CustomerCountry> customers = new List<CustomerCountry>();
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT COUNT(CustomerID) as Customers, Country " +
                    "FROM Customer " +
                    "GROUP BY Country " +
                    "ORDER BY COUNT(CustomerID) DESC";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CustomerCountry customerCountry = new CustomerCountry
                    {
                        CustomerCount = reader.GetInt32(0),
                        CountryName = reader.GetString(1)
                    };
                    customers.Add(customerCountry);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customers;
        }
    }
}
