﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Interface for the CustomerSpenderRepository.
    /// </summary>
    public interface ICustomerSpenderRepository
    {
        IEnumerable<CustomerSpender> GetCustomerSpender();
    }
}
