﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Interface for the CustomerCountryRepository.
    /// </summary>
    public interface ICustomerCountryRepository
    {
        IEnumerable<CustomerCountry> GetCustomerCountry();
    }
}
