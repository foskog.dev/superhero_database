﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Main repository for dealing with customer objects.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// This method returns every customer from the chinook database in a list.
        /// </summary>
        /// <returns>List of customers</returns>
        public IEnumerable<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, '') as Country, ISNULL(PostalCode, '') as PostalCode, ISNULL(Phone, '') as Phone, Email " +
                  "FROM Customer";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Customer customer = new Customer
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.GetString(3),
                        PostalCode = reader.GetString(4),
                        Phone = reader.GetString(5),
                        Email = reader.GetString(6),
                    };
                    customers.Add(customer);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customers;
        }

        /// <summary>
        /// Gets a single customer based on the id that is passed into the method. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer object that matches id.</returns>
        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, '') as Country, ISNULL(PostalCode, '') as PostalCode, ISNULL(Phone, '') as Phone, Email " +
                  "FROM Customer WHERE CustomerId = @CustomerId";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", id);

                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customer.Id = reader.GetInt32(0);
                    customer.FirstName = reader.GetString(1);
                    customer.LastName = reader.GetString(2);
                    customer.Country = reader.GetString(3);
                    customer.PostalCode = reader.GetString(4);
                    customer.Phone = reader.GetString(5);
                    customer.Email = reader.GetString(6);
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customer;
        }

        /// <summary>
        /// Gets a customer based on their name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Customer object based on name.</returns>
        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, '') as Country, ISNULL(PostalCode, '') as PostalCode, ISNULL(Phone, '') as Phone, Email " +
                  "FROM Customer WHERE FirstName LIKE @FirstName";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@FirstName", name);

                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customer.Id = reader.GetInt32(0);
                    customer.FirstName = reader.GetString(1);
                    customer.LastName = reader.GetString(2);
                    customer.Country = reader.GetString(3);
                    customer.PostalCode = reader.GetString(4);
                    customer.Phone = reader.GetString(5);
                    customer.Email = reader.GetString(6);
                };

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customer;
        }

        /// <summary>
        /// This method returns a limited list of customers from the chinook database.
        /// the offset and count parameters sets how far down the list you want to limit and how many results to return.
        /// </summary>
        /// <returns>List of customers</returns>
        public IEnumerable<Customer> GetSubsetOfCustomers(int offset, int count)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, '') as Country, ISNULL(PostalCode, '') as PostalCode, ISNULL(Phone, '') as Phone, Email " +
                  "FROM Customer " +
                  "ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Count ROWS ONLY";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@Offset", offset);
                command.Parameters.AddWithValue("@Count", count);

                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Customer customer = new Customer
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.GetString(3),
                        PostalCode = reader.GetString(4),
                        Phone = reader.GetString(5),
                        Email = reader.GetString(6),
                    };
                    customers.Add(customer);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customers;
        }

        /// <summary>
        /// Insert a new customer to the database. Takes a predefined customer object containing the users information.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>true on success false on failure.</returns>
        public bool InsertCustomer(Customer customer)
        {
            bool success = false;
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                string sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email) " +
                "VALUES(@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);
                success = command.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        /// <summary>
        /// Updates an existing customer. Takes the id of the customer you want to update. and a customer object containing the new values
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customer"></param>
        /// <returns>true on success false on failure.</returns>
        public bool UpdateCustomer(int id, Customer customer)
        {
            bool success = false;
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                string sql = "UPDATE Customer " +
                    "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                    "WHERE CustomerId = @CustomerId";

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", id);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);
                
                success = command.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

    }
}
