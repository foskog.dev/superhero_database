﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Interface for the customer repository.
    /// </summary>
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomerById(int id);
        Customer GetCustomerByName(string name);
        IEnumerable<Customer> GetSubsetOfCustomers(int offset, int count);
        bool InsertCustomer(Customer customer);
        bool UpdateCustomer(int id, Customer customer);
    }
}
