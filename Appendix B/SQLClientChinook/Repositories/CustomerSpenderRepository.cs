﻿using SQLClientChinook.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientChinook.Repositories
{
    /// <summary>
    /// Repository for CustomerSpender objects. Used to get a list of customers ordered by how much they have spent.
    /// </summary>
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Returns a list of customerIds sorted on their spending habits. Sorted from those that have spent most to those that have spent less.
        /// </summary>
        /// <returns>List of CustomerSpender Objects.</returns>
        public IEnumerable<CustomerSpender> GetCustomerSpender()
        {

            List<CustomerSpender> customers = new List<CustomerSpender>();
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                var sql = "SELECT CustomerId, SUM(Total) as Total FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CustomerSpender customerSpender = new CustomerSpender
                    {
                        CustomerId = reader.GetInt32(0),
                        TotalSpent = reader.GetDecimal(1)
                    };
                    customers.Add(customerSpender);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customers;
        }
    }
}
