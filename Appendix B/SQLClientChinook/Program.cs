﻿using SQLClientChinook.Models;
using SQLClientChinook.Repositories;
using System;
using System.Collections;

namespace SQLClientChinook
{
    public class Program
    {

        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            
            // Get all customers
            var getAllCustomersResult = repository.GetAllCustomers();
            Console.WriteLine("all customers");
            foreach (var customer in getAllCustomersResult)
            {
                Console.WriteLine(customer.Id);
                Console.WriteLine(customer.FirstName);
                Console.WriteLine(customer.LastName);
                Console.WriteLine("--------------");
            };

            // get single customer by id
            var getSingleCustomerByIdResult = repository.GetCustomerById(5);
            Console.WriteLine("Customer by id");
            Console.WriteLine(getSingleCustomerByIdResult.Id);
            Console.WriteLine(getSingleCustomerByIdResult.FirstName);
            Console.WriteLine(getSingleCustomerByIdResult.LastName);
            Console.WriteLine("--------------");

            // get single customer by name
            var getSingleCustomerByNameResult = repository.GetCustomerByName("Steve");
            Console.WriteLine("Customer by name");
            Console.WriteLine(getSingleCustomerByNameResult.Id);
            Console.WriteLine(getSingleCustomerByNameResult.FirstName);
            Console.WriteLine(getSingleCustomerByNameResult.LastName);
            Console.WriteLine("-------------");

            // Get subset of customers
            var getSubsetOfCustomers = repository.GetSubsetOfCustomers(5, 5);
            Console.WriteLine("Subset of customers");
            foreach (var customer in getSubsetOfCustomers)
            {
                Console.WriteLine(customer.Id);
                Console.WriteLine(customer.FirstName);
                Console.WriteLine(customer.LastName);
                Console.WriteLine("---------------");
            }

            // Insert new customer
            Console.WriteLine("Insert new customer");
            Customer newCustomer = new Customer { 
                FirstName = "Ky",
                LastName = "Kiske",
                Country = "France",
                PostalCode = "236P",
                Phone = "6321456",
                Email = "WakeUpSuper@mail.com" 
            };
            if(repository.InsertCustomer(newCustomer))
            {
                Console.WriteLine("Customer added successfully.");
            } else {
                Console.WriteLine("Error adding customer");
            }
            Console.WriteLine("--------------");

            // Update Customer
            Console.WriteLine("Update exsiting customer");
            Customer updateCustomer = new Customer
            {
                FirstName = "Ky",
                LastName = "Kiske",
                Country = "France",
                PostalCode = "623S",
                Phone = "4123654",
                Email = "Stundipper@mail.com"
            };
            if(repository.UpdateCustomer(56, updateCustomer))
            {
                Console.WriteLine("Customer updated successfully");
            } else
            {
                Console.WriteLine("Error updating customer");
            }
            Console.WriteLine("------------");

            ICustomerCountryRepository countryRepository = new CustomerCountryRepository();

            var getCustomerCountry = countryRepository.GetCustomerCountry();
            Console.WriteLine("Customers by country");
            foreach (var customerCountry in getCustomerCountry)
            {
                Console.WriteLine(customerCountry.CountryName);
                Console.WriteLine(customerCountry.CustomerCount);
                Console.WriteLine("-------------");
            }

            ICustomerSpenderRepository spenderRepository = new CustomerSpenderRepository();

            var getCustomerSpender = spenderRepository.GetCustomerSpender();
            Console.WriteLine("Customers by amount spent");
            foreach ( var customerSpender in getCustomerSpender)
            {
                Console.WriteLine(customerSpender.CustomerId);
                Console.WriteLine(customerSpender.TotalSpent);
                Console.WriteLine("-------------");
            }

            ICustomerGenreRepository genreRepository = new CustomerGenreRepository();

            var getCustomerGenreWithTie = genreRepository.GetCustomerTopGenre(5);
            Console.WriteLine("CustomerGenre with tie");
            foreach ( var customerGenreWithTie in getCustomerGenreWithTie)
            {
                Console.WriteLine(customerGenreWithTie.CustomerId);
                Console.WriteLine(customerGenreWithTie.GenreId);
                Console.WriteLine(customerGenreWithTie.GenreName);
                Console.WriteLine("-----------");
            }

            var getCustomerGenreWithoutTie = genreRepository.GetCustomerTopGenre(4);
            Console.WriteLine("CustomerGenre without tie");
            foreach (var customerGenreWithoutTie in getCustomerGenreWithoutTie)
            {
                Console.WriteLine(customerGenreWithoutTie.CustomerId);
                Console.WriteLine(customerGenreWithoutTie.GenreId);
                Console.WriteLine(customerGenreWithoutTie.GenreName);
                Console.WriteLine("-----------");
            }
        }
    }
}