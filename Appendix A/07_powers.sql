USE SuperheroesDb

GO

INSERT INTO Power (Name, Description) VALUES ('Super Strength', 'Is super strong. Does not require a forklift.')
INSERT INTO Power (Name, Description) VALUES ('Flight', 'Naturally occouring or enabled by jetplane. Is there really a difference?')
INSERT INTO Power (Name, Description) VALUES ('Super Techyness', 'Speaks fluent ones and zeroes.')
INSERT INTO Power (Name, Description) VALUES ('Immense wealth', 'Cash is king')

INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (1, 3)
INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (2, 2)
INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (2, 3)
INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (2, 4)
INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (3, 1)
INSERT INTO Superhero_Power (Superhero_Id, Power_Id) VALUES (3, 2)

SELECT * FROM Superhero
SELECT * FROM Assistant
SELECT * FROM Power
SELECT * SELECT Superhero_Power

SELECT * FROM Superhero AS s
INNER JOIN Superhero_Power AS s_p ON s.Id = s_p.Superhero_Id
INNER JOIN Power AS p ON p.Id = s_p.Power_Id
