USE SuperheroesDb

GO

CREATE Table Superhero(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50),
	Alias nvarchar(50),
	Origin text
)

CREATE Table Assistant(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50),
)

CREATE Table Power(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50),
	Description text
)