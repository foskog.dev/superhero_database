USE SuperheroesDb

GO

INSERT INTO Superhero (Name, Alias, Origin) VALUES ('Hackerman', 'Elliot Alderson', 'Will steal your password, Seeks to liberate society from some generic megacorporation.')

INSERT INTO Superhero (Name, Alias, Origin) VALUES ('Batman', 'Bruce Wayne', 'Rich beyond belief. Defends Gotham by spending billions on life size kids toys.')

INSERT INTO Superhero (Name, Alias, Origin) VALUES ('Superman', 'Clark Kent', 'Overpowered alien from space, could annihilate the planet on a whim. Elects not to.')

SELECT * FROM Superhero