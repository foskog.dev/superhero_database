USE SuperheroesDb

GO

ALTER TABLE Assistant
ADD Superhero_Id int NOT NULL

GO

ALTER TABLE Assistant
ADD CONSTRAINT fk_SuperheroId
FOREIGN KEY (Superhero_Id) REFERENCES Superhero(id)