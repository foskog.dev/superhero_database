USE SuperheroesDb

GO

CREATE TABLE Superhero_Power(
	Superhero_Id int,
	Power_Id int,
	CONSTRAINT PK_Superhero_Power PRIMARY KEY (Superhero_Id, Power_Id)
)

ALTER TABLE Superhero_Power
ADD FOREIGN KEY (Superhero_Id) REFERENCES Superhero(id)

ALTER TABLE Superhero_Power
ADD FOREIGN KEY (Power_Id) REFERENCES Power(id);